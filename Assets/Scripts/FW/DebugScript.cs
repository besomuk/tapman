﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;

using UnityEngine.UI;

public class DebugScript : MonoBehaviour
{
    public bool debugOn = true;
    public string separator = " -> ";

    public Text txtVersionLocal;
    public Text txtVersionServer;

    void Start()
    {
        if (debugOn)
            Debug.Log("-> Debug ON <-");

        if (txtVersionLocal)
            txtVersionLocal.text = "Lokal: " + Application.identifier + ", " + Application.version;
        if (txtVersionServer)
            txtVersionServer.text = "Server: ";

        Global.firebase.Test();
        //Global.firebase.GetServerInfo();
    }

    /// <summary>
    /// Stampa poruku ako je ukljucen debug. Ako se proslede 2 parametra, prvi parametar 
    /// je nekakav ID koji se npr moze upotrebiti kao filter za logcat ( npr, 'adb logcat | grep [filter]' )
    /// </summary>
    /// <param name="_str">String.</param>
    public void Print(string _str)
    {
        if (debugOn)
            Debug.Log(_str);
    }

    /// <summary>
    /// Stampa poruku ako je ukljucen debug. Ako se proslede 2 parametra, prvi parametar 
    /// je nekakav ID koji se npr moze upotrebiti kao filter za logcat ( npr, 'adb logcat | grep [filter]' )
    /// </summary>
    /// <param name="_id">Identifier.</param>
    /// <param name="_str">String.</param>
    public void Print (string _id, string _str)
    {
        if (debugOn)
            Debug.Log(_id + separator + _str);
    }

    public void PrintError(string _str)
    {
        Debug.LogError(_str);
    }

    public void PrintError(string _id, string _str)
    {
        Debug.LogError(_id + separator + _str);
    }

    /// <summary>
    /// Isto kao i Print(), samo sto stampa poruku bez obzira na debugOn status.
    /// Korisno ako treba da se ispise samo deo debuga. Isto se moze postici i sa Print(),
    /// ako se debugOn iskljuci, isprintuje, pa ukljuci, samo sto ovako lepse izgleda
    /// </summary>
    /// <param name="_str">String.</param>
    public void AlwaysPrint(string _str)
    {
        Debug.Log(_str);
    }

    public void AlwaysPrint(string _id, string _str)
    {
        Debug.Log(_id + separator + _str);
    }
}
