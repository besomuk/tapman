﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;
//using System;

public class FirebaseControler : MonoBehaviour
{
    public Firebase.FirebaseApp app;        // referenca na app
    public bool firebaseReady = false;      // flag spremnosti za firebase
    public string firebaseDatabaseURL = @"https://pokegafw3.firebaseio.com/"; // URL do Firebase baze
    public DatabaseReference reference;     // referenca na Firebase bazu
    public DataSnapshot snapshot;           // snapshot Firebase baze
    public Firebase.Auth.FirebaseAuth auth; // Firebase auth
    public Firebase.Auth.FirebaseUser user; // Firebase user
    public string firebaseUserID = "";             // ID korisnika, onaj koji dodeljuje Firebase

    public bool editorTestEnabled = false; // da li je ukljuceno testiranje iz editora?...
    public string editorTestId = "";       // Ako jeste, upotrebljava se ovaj ID. Najbolje
                                           // uzeti ID koji je napravio telefon, posto Firebase
                                           // ne da upisivanje bilo kakvih id-ova.
                                           // Ako nije, Firebase generise UID


    public string defaultDatabaseNode = "users"; // default node za bazu

    void Start()
    {
        // prvo pogledaj da li je sve za firebase ok. Ako jeste, inicijalizuj sta treba
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                app = Firebase.FirebaseApp.DefaultInstance;

                firebaseReady = true;

                InitializeFirebase();

                Global.debug.Print("XXKXX", "Firebase - spreman!");
            }
            else
            {
                Global.debug.Print("XXKXX", "Firebase - greska!");

                UnityEngine.Debug.LogError(System.String.Format("XXKXX Could not resolve all Firebase dependencies: {0}", dependencyStatus));

            }
        });
    }

    /// <summary>
    /// Inicijalizacija Firebase-a i podesavanja istog.
    /// </summary>
    public void InitializeFirebase()
    {
        Global.debug.Print("XXKXX", "Init Firebase....");

        // init baze
        app.SetEditorDatabaseUrl(firebaseDatabaseURL);
        reference = FirebaseDatabase.DefaultInstance.RootReference;

        // init auth
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        // anon ulaz
        auth.SignInAnonymouslyAsync().ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInAnonymouslyAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;

            if (editorTestEnabled)
            {
                Global.debug.Print("XXKXX", "*** Paznja, ukljucen je EDITOR MODE! ***");
                firebaseUserID = editorTestId;
            }
            else
                firebaseUserID = newUser.UserId;

            Debug.LogFormat("XXKXX -> User signed in successfully: {0} ({1})", newUser.DisplayName, firebaseUserID);

            // da li user vec postoji?
            CheckExistingAnonUser(defaultDatabaseNode, firebaseUserID, true);

        });
        // google

        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;
    }

    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        //UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
        Global.debug.Print("XXKXX", "Received Registration Token : " + token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        //UnityEngine.Debug.Log("Received a new message from: " + e.Message.From);
        Global.debug.Print("XXKXX", "Received a new message from: " + e.Message.From);
    }

    public void CheckExistingAnonUser(string _dbNode, string _userId, bool _autoWrite)
    {
        Global.debug.Print("XXKXX", "Provera usera u -> " + _dbNode + " <- ...");



        FirebaseDatabase.DefaultInstance.GetReference(_dbNode).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Global.debug.Print("XXKXX", "Error....");
            }
            else if (task.IsCompleted)
            {
                snapshot = task.Result;
                //firebaseUserID = "KCyZsHQTKmSdMRlolpetMru9RId2";

                bool existingUser = snapshot.Child(_userId).Exists;

                if (existingUser)
                {
                    Global.debug.Print("XXKXX", "Korisnik postoji u bazi.");
                    //if (_autoWrite)
                        LoadExistingUser(defaultDatabaseNode, _userId);
                }
                else
                {
                    Global.debug.Print("XXKXX", "Korisnik ne postoji u bazi.");
                    //if (_autoWrite)
                         CreateNewUser(defaultDatabaseNode, _userId);
                }
            }

        });


    }

    public void CreateNewUser(string _dbNode, string _userID)
    {
        //Player p = new Player( firebaseUserID, Tools.GetTimeString(), 1, true, true, true, 0 );
        Player p = new Player( firebaseUserID );
        Global.player = p;
        string json = JsonUtility.ToJson(p);

        reference.Child(_dbNode).Child(_userID).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Global.debug.Print("XXKXX", "Error writing to Firebase!");
            }
            else if (task.IsCompleted)
            {
                Global.debug.Print("XXKXX", "Write to Firebase OK!");
            }
        });
    }

    public void SetBestScore()
    {
        // load score
        // if > score -> nista
        // else < save
        FirebaseDatabase.DefaultInstance.GetReference( defaultDatabaseNode ).GetValueAsync().ContinueWith( task =>
        {
            if ( task.IsFaulted )
            {
                Global.debug.Print( "XXKXX", "Error loading user!" );
            }
            else if ( task.IsCompleted )
            {
                snapshot = task.Result;
                
                int currentBestScore = int.Parse( snapshot.Child( Global.player.uid ).Child( "bestScore" ).Value.ToString() );

                Global.player.currentScore = 600;

                if ( Global.player.currentScore > currentBestScore )
                {
                    Global.player.bestScore = Global.player.currentScore;
                    SaveData( Global.player, defaultDatabaseNode, Global.player.uid );
                    //Global.ui.UpdateUI();
                }
                else
                {
                    Global.debug.Print( "XXKXX", "Ne treba" );
                }
                
            }
        } );
    }


    public void Test()
    {
        Debug.Log( "Test" );
    }

    public void LoadExistingUser(string _dbNode, string _userID)
    {
        //Global.debug.Print("XXKXX", "Load Existing user");
        FirebaseDatabase.DefaultInstance.GetReference(_dbNode).GetValue().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Global.debug.Print("XXKXX", "Error loading user!");
            }
            else if (task.IsCompleted)
            {
                snapshot = task.Result;

                Global.debug.Print("XXKXX", "Trying to load existing user 1...");

                string uid = snapshot.Child( _userID ).Child( "uid" ).Value.ToString();
                int timesPlayed = int.Parse( snapshot.Child( _userID ).Child( "timesPlayed" ).Value.ToString() );
                int bestScore   = int.Parse( snapshot.Child( _userID ).Child( "bestScore" ).Value.ToString() );
                string lastSeen = snapshot.Child(_userID).Child("lastSeen").Value.ToString();
                bool music = System.Convert.ToBoolean( snapshot.Child( _userID ).Child( "music" ).Value.ToString() );
                bool fx = System.Convert.ToBoolean( snapshot.Child( _userID ).Child( "fx" ).Value.ToString() );
                bool notifications = System.Convert.ToBoolean( snapshot.Child( _userID ).Child( "notifications" ).Value.ToString() );                

                Global.debug.Print( "XXKXX", "Trying to load existing user 2..." );

                //bool fx
                //bool notifications 
                timesPlayed++;
                lastSeen = Tools.GetTimeString();

                Global.player.uid          = uid;
                Global.player.timesPlayed  = timesPlayed;
                Global.player.lastSeen     = lastSeen;
                Global.player.bestScore    = bestScore;
                Global.player.music        = music;
                Global.player.fx            = fx;
                Global.player.notifications = notifications;
                Global.debug.Print( "XXKXX", "Trying to load existing user 3..." );

                Global.debug.Print( "XXKXX", "Logovan score: " + bestScore );

                Global.debug.Print( "XXKXX", "Load done..." );
                SaveData(Global.player, defaultDatabaseNode, _userID);
                Global.debug.Print( "XXKXX", "Save done..." );

                PlayerPrefs.SetInt( "highscore", Global.player.bestScore );
            }
        });

        
    }

    /// <summary>
    /// Cuvanje podataka u Firebase bazi.
    /// </summary>
    public void SaveData(Player _p, string _dbNode, string _uid)
    {
        Global.debug.Print("XXKXX", "Save data....");

        Player p = _p;

        string json = JsonUtility.ToJson(_p);

        reference.Child(_dbNode).Child(_uid).SetRawJsonValueAsync(json);
    }
    

    /// <summary>
    /// Ucitavanje podataka iz Firebase baze. Vise ne koristim, podaci se sada ucitavaju
    /// iz LoadExistingUser() i to na potpuno isti nacin. Ovu ostavljam radi primera i moze se obrisati
    /// </summary>
    public void LoadData(string _dbNode)
    {
        Global.debug.Print("XXKXX", "Load data....");

        FirebaseDatabase.DefaultInstance.GetReference(_dbNode).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Global.debug.Print("XXKXX", "Error....");
            }
            else if (task.IsCompleted)
            {
                snapshot = task.Result;
                //string choban = snapshot.Child("4088").Child("timesPlayed").Value.ToString();
                //Global.debug.Print("XXKXX", "Vidim: " + choban);
            }
        });
    }

    void HandleValueChanged(object sender, ValueChangedEventArgs e)
    {
        Global.debug.Print("XXKXX", "Desila se promena u bazi...");
        Global.debug.Print("XXKXX", e.Snapshot.GetRawJsonValue());
    }


    /// <summary>
    /// Update podataka u Firebase bazi. Ovo je primer update-a za nod sa id-em
    /// 9725 i podatka timesPlayed koji je vezan za taj nod
    /// </summary>
    public void UpdateData(string _dbNode)
    {
        Global.debug.Print("XXKXX", "Update data....");
        reference.Child(_dbNode).Child("9725").Child("timesPlayed").SetValueAsync(Random.Range(1, 1001));
    }

    public void TestData()
    {
        Global.debug.Print( "XXKXX", "Writing test data..." );
        reference.Child( "users" ).Child( "space" ).Child( "timesPlayed" ).SetValueAsync( Random.Range( 1, 1001 ) );
    }

    public void GetServerInfo()
    {
        Global.debug.Print("XXKXX", "Get server info....");

        FirebaseDatabase.DefaultInstance.GetReference("info").GetValueAsync().ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Global.debug.Print("XXKXX", "Error....");
            }
            else if (task.IsCompleted)
            {
                snapshot = task.Result;
                string releaseDate = snapshot.Child("releaseDate").Value.ToString();
                string version     = snapshot.Child("version").Value.ToString();

                Global.debug.Print("XXKXX", "Release date: " + releaseDate);
                Global.debug.Print("XXKXX", "Version: " + version);
            }
        });
    }
}
