﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class Tools
{
    /// <summary>
    /// Vraca datum i vreme u formatu [YYYY]-[MM]-[DD]-[HH]-[MM]-[SS]
    /// </summary>
    /// <returns>The time string.</returns>
    public static string GetTimeString()
    {
        string s = "-";

        DateTime dt = new DateTime();
        dt = DateTime.UtcNow;

        string dateTimeFinal;

        dateTimeFinal  = dt.Year + s + dt.Month.ToString("00") + s + dt.Day.ToString("00") + s + 
                           dt.Hour.ToString("00") + s + dt.Minute.ToString("00") + s + dt.Second.ToString("00") + s + dt.Millisecond.ToString("000"); ;

        return dateTimeFinal;
    }
}
