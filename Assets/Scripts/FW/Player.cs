﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    public string uid;         // UID koji se dobija od Firebase.
    public bool music;         // ukljucena / iskljucena muzika
    public bool fx;            // ukljucen / iskljucen zvuk
    public bool notifications; // ukljucene iskljucene notifikacije
    public string lastSeen;    // kad si vidjen poslednji put online, UTC 
    public int timesPlayed;    // koliko puta si otvorio app
    public int currentScore;   // koliko puta si otvorio app
    public int bestScore;      // koliko puta si otvorio app

    // inicijalizacija
    public Player()
    {
        this.uid = "space";
        this.music = true;
        this.fx = true;
        this.notifications = true;
        this.lastSeen = "never";
        this.timesPlayed = 1;
        this.currentScore = 0;
        this.bestScore = 0;
    }

    /// <summary>
    /// Pravljenje novof igraca sa uid-om i default podacima
    /// </summary>
    /// <param name="uid"></param>
    public Player( string uid )
    {
        this.uid = uid;
        this.music = true;
        this.fx = true;
        this.notifications = true;
        this.lastSeen = Tools.GetTimeString();
        this.timesPlayed = 1;
        this.currentScore = 0;
        this.bestScore = 0;
    }

    /// <summary>
    /// Konstruktor koji pravi Playera / Usera. Od ovoga se pravi JSON koji 
    /// ce biti upucan u Firebase bazu.
    /// </summary>
    /// <param name="uid">Uid.</param>
    /// <param name="lastSeen">Last seen.</param>
    /// <param name="timesPlayed">Times played.</param>
    public Player(string uid, string lastSeen, int timesPlayed, bool music, bool fx, bool notifications, int bestScore )
    {
        this.uid = uid;
        this.music = music;
        this.fx = fx;
        this.notifications = notifications;
        this.lastSeen = lastSeen;
        this.timesPlayed = timesPlayed;
        this.bestScore = bestScore;
    }

    /// <summary>
    /// Cuvanje u lokalnoj bazi, na uredjaju.
    /// Ako je korisnik ulogovan na FB, onda podatke pise i na Firebase
    /// </summary>
    public void SaveLocal()
    {
        Global.debug.Print("XXKXX", "Save Local...");

        PlayerPrefs.SetString("uid", this.uid);
        PlayerPrefs.SetString("music", this.music.ToString());
        PlayerPrefs.SetString("fx", this.fx.ToString());
        PlayerPrefs.SetString("notifications", this.notifications.ToString());
        PlayerPrefs.SetString("lastSeen", this.lastSeen);
        PlayerPrefs.SetInt("timesPlayer", this.timesPlayed);

        //if (Global.facebook.isLogged)
        //{
        //    Global.firebase.SaveData(Global.player, "users", Global.firebase.firebaseUserID);
        //}
        PrintPlayerData();
    }

    /// <summary>
    /// Ucitavanje podataka iz lokalne baze
    /// </summary>
    public void LoadLocal()
    {
        Global.debug.Print("XXKXX", "Load Local...");

        this.uid           = PlayerPrefs.GetString("uid");

        // uzmi true/false podatke. mora ovako zbog konverzije tipova
        if (PlayerPrefs.GetString("music") == "True")
            this.music = true;
        else
            this.music = false;

        if (PlayerPrefs.GetString("fx") == "True")
            this.fx = true;
        else
            this.fx = false;

        if (PlayerPrefs.GetString("notifications") == "True")
            this.notifications = true;
        else
            this.notifications = false;

        this.lastSeen      = PlayerPrefs.GetString("lastSeen");
        this.timesPlayed   = PlayerPrefs.GetInt("timesPlayer");

        //Global.ui.UpdateUISettings();

        PrintPlayerData();
    }

    /// <summary>
    /// Ispis podataka u konzolu, testa radi
    /// </summary>
    public void PrintPlayerData()
    {
        Global.debug.Print("XXKXX", " --------------------------------- ");
        Global.debug.Print("XXKXX", " music         - " + this.music);
        Global.debug.Print("XXKXX", " fx            - " + this.fx);
        Global.debug.Print("XXKXX", " notifications - " + this.notifications);
        Global.debug.Print("XXKXX", " --------------------------------- ");
    }
}
