﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Global : MonoBehaviour
{
    public static Global instance;

    public static Player player;                // player skripta -> cuva podatke o trenutnom playeru useru, 
                                                // one koji ce upisivati u bazu 
    public static FirebaseControler firebase;        // firebase kontroler
    public static DebugScript debug;                 // debug skripta
    //public static NativeShareController nativeShare; // native share za uredjaje
    //public static AdMobController admob;             // admob kontroler
    //public static UnityAdController unityAd;         // unity ads kontroler
    //public static GPGSController GPGS;               // google play game services
    //public static FacebookController facebook;       // FB kontroler
    //public static LocalNotificationController localNotification; // lokalne notifikacije
    //public static FrameworkInfo fwinfo; // frame work info

    // Start is called before the first frame update
    void Awake()
    {
        if ( instance == null )
            instance = this;

        player = new Player();

        firebase = GetComponent<FirebaseControler>();
        debug = GetComponent<DebugScript>();
        //nativeShare = GetComponent<NativeShareController>();
        //admob       = GetComponent<AdMobController>();
        //unityAd     = GetComponent<UnityAdController>();
        //GPGS        = GetComponent<GPGSController>();
        //facebook          = GetComponent<FacebookController>();
        //localNotification = GetComponent<LocalNotificationController>();
        //fwinfo            = GetComponent<FrameworkInfo>();
        //inventory         = GetComponent<Inventory>();

        //player.LoadLocal();

        player.bestScore = PlayerPrefs.GetInt( "highscore" );
        UIManager.instance.UpdateUI();

    }


    private void Start()
    {
        // Ucitaj lokalna podesavanja
        //player.LoadLocal();

        //inventory.GetAllItems();
        //inventory.ShowItems();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnApplicationFocus( bool hasFocus )
    {
        //if (!hasFocus)
        //    localNotification.ScheduleNotification("Ej bre", "Hajde igra malo", System.TimeSpan.FromSeconds(10));
    }

    IEnumerator WaitForFireBase()
    {
        yield return new WaitForSeconds( 5 );
    }

}
